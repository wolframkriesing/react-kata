# react kata

Goal of this kata is to investigate how much view and business logic are entangled.
If it is possible to separate them my personal goal is achieved.

## The Kata

Unlock/lock a car

1. click the "unlock" button, send a fetch request to the backend
   while inflight show "unlocking...", when done show "sent", on failure "failed"
1. timeout 
1. backend data push
